#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: Analytical 2D analysis of a stator with concentrated windings
#Author : Andreas Joss

from __future__ import division

import pylab as pl
import numpy as np
import time

show_plot_1D = False
show_plot_2D = True
enable_1D_analysis = True
enable_2D_analysis = True

LINEWIDTH=1
max_harmonics = 49 #should be 49

range_pos = []
range_neg = []
range_all = []

for i in range (0,int(max_harmonics/2)):
  range_pos.append(1+(i*6))
  
  if 5+(i*6) >=max_harmonics:
    break
  if 1+(i*6) >=max_harmonics:
    break

  range_neg.append(5+(i*6))

for i in range (0,len(range_pos)):
  range_all.append(range_pos[i])
  if i < len(range_neg):
    range_all.append(range_neg[i])

#now convert range_all array to numpy array
range_all = np.array(range_all)

#################################
# define variables to be solved #
#################################
b_I   = np.zeros((len(range_all)+1,1))
c_II  = np.zeros((len(range_all)+1,1))
d_II  = np.zeros((len(range_all)+1,1))
c_III = np.zeros((len(range_all)+1,1))
d_III = np.zeros((len(range_all)+1,1))
    
######################
# machine parameters #
######################
p = 1
N = 100
Imax = np.sqrt(2)*10
rn = 100e-3
hy = 50e-3
mu0 = 4*np.pi*1E-7
muR = 1000
mu_I   = muR
mu_II  = 1
mu_III = muR
g = 2e-3
angular_freq = 50
q = 1 #coils per phase

#######################
# auxiliary variables #
#######################
r_i   = rn - g/2
r_ii  = rn + g/2
r_iii = rn + g/2 + hy

#normalize radii with rn, so that higher harmonics are computationally easier to compute, and that possible overflowing/underflowing is prevented
r_i = r_i/rn
r_ii = r_ii/rn
r_iii = r_iii/rn

dr=r_iii/600
#r_range=np.arange(r_i,r_iii,dr)
r_range=np.arange(dr,r_iii,dr)

M = 128 #In order to utilise the FFT optimally later on
phi_range=np.linspace(-np.pi/q,np.pi/q,int(M*2))

####################    
# define solutions #    
####################
Az=np.zeros((r_range.size,phi_range.size))
Br=np.zeros((r_range.size,phi_range.size))
Bt=np.zeros((r_range.size,phi_range.size))
B_magnitude=np.zeros((r_range.size,phi_range.size))

########################
# function definitions #
########################
def lookup(ref,array,tol):
  r'''A lookup function to determine the index value of a specific floating point value in a floating point array,
due to the fact that a floating point value of say "0.232" is represented in python as "0.23200000000000001"'''
  for i,val in enumerate(array):
    if abs((val-ref))<tol:
      return i


#calculates Az for a certain radius and theta, up to the maximum allowed harmonic
def magnetic_vector_potential (radius,theta,t):		 #(Az) exists only in z direction
  temp = 0
  i = 0
  
  #print("value of p is %d"%p)
  
  if radius <= r_i:	#REGION 1
    for n in np.nditer(range_all):
      temp = temp  + b_I[i+1]*(radius**(n*p))*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
  elif radius <= r_ii:	#REGION 2
    for n in np.nditer(range_all):
      temp = temp  + ((c_II[i+1]*(radius**(n*p))) + (d_II[i+1]*(radius**(-n*p))))*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
  elif radius <= r_iii: #REGION 3
    for n in np.nditer(range_all):
      temp = temp  + ((c_III[i+1]*(radius**(n*p))) + (d_III[i+1]*(radius**(-n*p))))*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1  

  return temp

def radial_flux_density (radius, theta, t):
  temp = 0
  i = 0
  
  if radius <= r_i:	#REGION 1
    for n in np.nditer(range_all):
      temp = temp  + b_I[i+1]*(radius**(n*p))*n*np.cos(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = (temp*p)/radius

  elif radius <= r_ii:	#REGION 2
    for n in np.nditer(range_all):
      temp = temp  + ((c_II[i+1]*(radius**(n*p))) + (d_II[i+1]*(radius**(-n*p))))*n*np.cos(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = (temp*p)/radius

  elif radius <= r_iii: #REGION 3
    for n in np.nditer(range_all):
      temp = temp  + ((c_III[i+1]*(radius**(n*p))) + (d_III[i+1]*(radius**(-n*p))))*n*np.cos(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = (temp*p)/radius

  return temp

def azimuthal_flux_density (radius, theta, t):
  temp = 0
  i = 0
  
  if radius <= r_i:	#REGION 1
    for n in np.nditer(range_all):
      temp = temp  + b_I[i+1]*(radius**(n*p))*n*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = -(temp*p)/radius

  elif radius <= r_ii:	#REGION 2
    for n in np.nditer(range_all):
      temp = temp  + ((c_II[i+1]*(radius**(n*p))) - (d_II[i+1]*(radius**(-n*p))))*n*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = -(temp*p)/radius

  elif radius <= r_iii: #REGION 3
    for n in np.nditer(range_all):
      temp = temp  + ((c_III[i+1]*(radius**(n*p))) - (d_III[i+1]*(radius**(-n*p))))*n*np.sin(n*p*theta + direction(n)*angular_freq*t)
      i = i + 1
    temp = -(temp*p)/radius
    
  return temp


def direction(n):
  if n%6 == 1:
    return +1
  elif (n+1)%6 == 0:
    return -1
  
  
####################
# matrix equations #
####################

start_time = time.time()
i = 0
#for n in range (1,max_harmonics+1):
for n in np.nditer(range_all):

  A = np.mat([  [1        	,  -1        			,  -r_i**(-2*n*p)		,  0				,  0		],
		[0		,  r_ii**(2*n*p)		,  1       			, -r_ii**(2*n*p)		,  -1    	],
		[mu_II		, -mu_I				,  mu_I*r_i**(-2*n*p)		,  0				,  0    	],
		[0		,  mu_III*r_ii**(2*n*p)		,  -mu_III			, -mu_II*r_ii**(2*n*p)		,  mu_II	],
		[0		,  0				,  0				, r_iii**(2*n*p)		,  1		]])

  B = np.mat([  [ 0  ],
		[ 0  ],
		[ 0  ],
		[ (4*N/np.pi)*Imax*np.sin(n*np.pi/2)*(3/2)*(1/2)*mu0*mu_II*mu_III*r_ii**(n*p-1) ],
		[ 0  ]])


  X=np.linalg.solve(A,B)


  b_I[i] 	= X[0,0]
  c_II[i]	= X[1,0]
  d_II[i]	= X[2,0]
  c_III[i]	= X[3,0]
  d_III[i]	= X[4,0]

  i = i + 1


start2_time = time.time()

if enable_2D_analysis == True:
  # calculate Az at iterations of the radius, and an even number of points around the entire machine (for improved FFT use)
  t = 0
  for i,r in enumerate(r_range):
    for y,phi in enumerate(phi_range):
      Az[i,y] = magnetic_vector_potential(r,phi,t)
      Br[i,y] = radial_flux_density(r,phi,t)
      Bt[i,y] = azimuthal_flux_density(r,phi,t)
      B_magnitude[i,y] = np.sqrt(Br[i,y]**2+Bt[i,y]**2)
      
  # plot radial flux density found in the airgap at t = 0
  #Get index for the following radii
  i_rn = lookup(rn/rn,r_range,dr/2)

  B_rn_new = np.zeros(np.shape(Br[i_rn]))
  for i, phi in enumerate(phi_range):
    B_rn_new[i] = radial_flux_density(rn/rn,phi,0)

  print("Time for 1st section: %f\nTime for 2nd section: %f\n"%(start2_time-start_time,time.time()-start2_time))
  
  print "2D analysis results follow"
  print("Br@rn_(max) = %g T" %max(Br[i_rn]))
  print('Br@rn_(avg) = %g T' % np.average(Br[i_rn]))
  


######################
# 1D Analysis Method #
######################
if enable_1D_analysis == True:
  print "\n1D analysis results follow"

  F_ag = np.zeros((phi_range.size,1))
  H_ag = np.zeros((phi_range.size,1))
  B_ag = np.zeros((phi_range.size,1))

  for i,phi in enumerate(phi_range):
    temp = 0
    #calculate MMF in the airgap (up to 49th harmonic)
    for n in np.nditer(range_all):
      temp = temp + (np.sin((n*np.pi)/2)*np.cos(n*phi))/n

    F_ag[phi] = (4*N*Imax*temp)/(2*np.pi)

    #calculate magnetic field intensity (H) (up to 49th harmonic)
    H_ag[phi] = F_ag[phi]/g

    #calculate magnetic flux density (B) (up to 49th harmonic)
    B_ag[phi] = mu0*H_ag[phi]

  print("B_ag(max) = %g T" %max(B_ag))
  print("B_ag(avg) = %g T" %np.average(B_ag)) #should be zero because inflowing flux should cancel out with outflowing flux
  print("B_ag(phi=0) = %g T" %B_ag[0])

  if show_plot_1D == True:
    pl.figure()
    pl.title(r'$B_r(\phi)$')
    pl.plot(np.degrees(phi_range),B_ag,linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$')

    ax=pl.gca()
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
    pl.ylabel(r'Radial flux density, $B_{r}$ [T]')
    pl.legend(loc='upper right')
    pl.axis(xmin=-180/q,xmax=180/q)
    pl.grid(True)

    pl.show()    


if show_plot_2D == True:
  #convert cylyndrical coordinates to cartesian coordinates
  # Initialise x & y array
  x=np.zeros(np.shape(Az))
  y=np.zeros(np.shape(Az))

  for i, r in enumerate(r_range):
    x[i] = r*rn*np.cos(phi_range)
    y[i] = r*rn*np.sin(phi_range)


  colorbar_shrink=0.25
  colorbar_aspect=10
  contour_figsize=(8,8)  

  # plot magnetic vector potential
  pl.figure(figsize=contour_figsize)
  CS = pl.contour(x, y, Az)
  pl.clabel(CS, inline=True, fontsize=5, fmt='%g')
  cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
  cb.set_label(r'Magnetic vector potential, $A_{z}$ [mWb/m]')
  #pl.title('Magnetic vector potential')
  pl.xlabel(r'$x$ [m]')
  pl.ylabel(r'$y$ [m]')

  # plot total magnetic flux density
  pl.figure(figsize=contour_figsize)
  CS=pl.contour(x,y,B_magnitude,40)
  CS=pl.contourf(x,y,B_magnitude,40)
  cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.2f')
  cb.set_label(r'Flux density, $B_{mag}$ [T]')
  pl.xlabel(r'$x$ [m]')
  pl.ylabel(r'$y$ [m]')



  pl.figure()
  pl.title(r'$B_r(\phi)$')
  pl.plot(np.degrees(phi_range),Br[i_rn],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}1}$')
  pl.plot(np.degrees(phi_range),B_rn_new,linewidth=LINEWIDTH,label=r'$B_{r|r_{n}2}$')

  ax=pl.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
  pl.ylabel(r'Radial flux density, $B_{r}$ [T]')
  pl.legend(loc='upper right')
  pl.axis(xmin=-180/q,xmax=180/q)
  pl.grid(True)

  pl.show()